from firedrake import *
import matplotlib
matplotlib.use("PDF")
import matplotlib.pyplot as plt
from myvi import *
import numpy
import os

class Beam1DProblem(MYVIProblem):
    def __init__(self):
        self.n = 100
        self.basen = 100

    def finite_element(self, cell):
        ele = FiniteElement("Hermite", cell, 3)
        return ele

    def base_mesh(self, comm):
        self.L = 1 # diameter of domain
        return UnitIntervalMesh(self.basen, comm=comm, distribution_parameters=dpnone)

    def initial_guesses(self, V, params):
        return [Function(V)]

    def energy(self, u, params):
        (B, P, rho, g) = map(Constant, params)
        v = u.dx(0)
        w = v.dx(0)
        J = (
            + 0.5 * B * inner(w, w)*dx
            - 0.5 * P * inner(v, v)*dx
            - rho*g*u*dx
            )
        return J
 

    def penalty(self, u, lb, ub, gamma, params):
        return Constant(gamma)/2 * (plus(lb-u)**2*dx + plus(u - ub)**2*dx)

    def boundary_conditions(self, V, params):
        return [DirichletBC(V, 0, "on_boundary")]

    def bounds(self, mesh, params):
        return (Constant(-0.4), Constant(+0.4))

    def solver_parameters(self, gamma, nref, params):
        sp = {
              "snes_monitor": None,
              "snes_type": "newtonls",
              "snes_converged_reason": None,
              "snes_max_it": 100,
              "snes_stol": 0,
              "snes_rtol": 1.0e-8,
              "snes_atol": 1.0e-5,
              "snes_linesearch_type": "basic",
              "ksp_type": "gmres", # iterative refinement
              "ksp_rtol": 1.0e-10,
              "ksp_atol": 0,
              "ksp_gmres_restart": 1,
              "mat_type": "matfree",
              "pmat_type": "aij",
              "pc_type": "lu"
             }
        return sp

    def refine(self, mesh, gamma):
        spacing = self.L/self.n
        invsqgm = 1/sqrt(gamma)

        if invsqgm < spacing:
            self.n *= 2
            return True
        else:
            return False

    def build_prolongation(self, ele, params):
        self.dgprolong = EmbeddedProlongation(ele)

    def prolong(self, u, v, params):
        self.dgprolong.prolong(u, v)

    def restrict(self, u, v, params):
        self.dgprolong.restrict(u, v)

    def inject(self, u, v, params):
        self.dgprolong.inject(u, v)

    def save_pvd(self, pvd, u):
        pass

    def save_solution(self, filename, num_solution, u, gamma, params):
        mesh = u.function_space().mesh()
        lag = FiniteElement("Lagrange", mesh.ufl_cell(), 3)
        Vlag = FunctionSpace(mesh, lag)
        plot(project(u, Vlag))
        (lb, ub) = self.bounds(mesh, params)
        x = numpy.linspace(0, 1, 101)
        plt.plot(x, [float(lb)]*len(x), "--r")
        plt.plot(x, [float(ub)]*len(x), "--r")
        plt.savefig(filename + "solution-%d.pdf" % num_solution)
        plt.clf()
        plt.close()

    def maximum_refinements(self, params):
        return 3

    def number_solutions(self, params):
        return 3

if __name__ == "__main__":

    B = 1
    P = 10.4
    rho = 1
    g = 1
    params = (B, P, rho, g)

    problem = Beam1DProblem()
    solutions = solvevi(problem, params)
    filename = "output/solutions/"
    os.makedirs(filename, exist_ok=True)

    for (i, soln) in enumerate(solutions):
        problem.save_solution(filename, i, soln, 1e6, params)
