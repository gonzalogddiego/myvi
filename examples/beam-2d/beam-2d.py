from firedrake import *
import firedrake
import matplotlib.pyplot as plt
from myvi import *
import numpy

class Beam2DProblem(MYVIProblem):
    def __init__(self):
        self.L = 1 # diameter of domain
        self.h = 0.1 # height

        self.basen = 40 # resolution of base mesh
        self.n = self.basen # resolution of current mesh

        self.biactivity = numpy.linspace(0, self.L, 1001) # for diagnostics

    def finite_element(self, cell):
        ele = VectorElement("Lagrange", cell, 1)
        return ele

    def base_mesh(self, comm):
        return RectangleMesh(self.basen, self.basen, self.L, self.h, comm=comm)

    def initial_guesses(self, V, params):
        mesh = V.mesh()
        x = SpatialCoordinate(mesh)
        guess = as_vector([-eps*x[0], 0])
        u = Function(V, name="Guess")
        u.interpolate(guess)
        return [u]

    def energy(self, u, params):
        B   = Constant((0.0, -1000)) # Body force per unit volume

        # Kinematics
        I = Identity(2)             # Identity tensor
        F = I + grad(u)             # Deformation gradient
        C = F.T*F                   # Right Cauchy-Green tensor

        # Invariants of deformation tensors
        Ic = tr(C)
        J  = det(F)

        # Elasticity parameters
        E, nu = 1000000.0, 0.3
        mu, lmbda = Constant(E/(2*(1 + nu))), Constant(E*nu/((1 + nu)*(1 - 2*nu)))

        # Stored strain energy density (compressible neo-Hookean model)
        psi = (mu/2)*(Ic - 2) - mu*ln(J) + (lmbda/2)*(ln(J))**2

        # Total potential energy
        J = psi*dx - dot(B, u)*dx

        return J

    def penalty(self, u, lb, ub, gamma, params):
        return Constant(1e4) * Constant(gamma)/2 * (plus(lb-u[1])**2*ds(3) + plus(u[1] - ub)**2*ds(4))

    def boundary_conditions(self, V, params):
        eps = params
        bcl = DirichletBC(V, Constant((0, 0)), 1)
        bcr = DirichletBC(V, Constant((-eps, 0)), 2)
        return [bcl, bcr]

    def bounds(self, mesh, params):
        return (Constant(-0.08), Constant(+0.08))

    def visualise_bounds(self):
        base = self.base_mesh(COMM_WORLD)
        ele = self.finite_element(base.ufl_cell())
        V = FunctionSpace(base, ele)
        (lbc, ubc) = self.bounds(base, None)

        lb = Function(V, name="LowerBound")
        lb.interpolate(as_vector([0, lbc - self.h]))
        ub = Function(V, name="UpperBound")
        ub.interpolate(as_vector([0, ubc + self.h]))

        leftwall = RectangleMesh(2, 2, self.h, 3*self.h + float(ubc) - float(lbc), comm=COMM_WORLD)
        leftwall.coordinates.dat.data[:, 1] += -self.h + float(lbc)
        leftwall.coordinates.dat.data[:, 0] += -self.h
        leftwallV = FunctionSpace(leftwall, "CG", 1)
        leftwallu = Function(leftwallV, name="LeftWall")
        return (lb, ub, leftwallu)

    def solver_parameters(self, gamma, nref, params):
        common = {
                 "snes_max_it": 150,
                 "snes_atol": 1.0e-8,
                 "snes_rtol": 1.0e-8,
                 "snes_stol": 0.0, # worst default in PETSc
                 "snes_max_linear_solve_fail": 150,
                 "snes_monitor": None,
                 "snes_converged_reason": None,
                 "snes_linesearch_type": "basic",
                 "snes_linesearch_maxstep": 1.0,
                 "snes_linesearch_damping": 1.0,
                 "snes_linesearch_monitor": None,
                 "ksp_max_it": 30,
                 "ksp_converged_reason": None,
                 }

        lu   = {
               "mat_type": "aij",
               "ksp_type": "preonly",
               "ksp_monitor_cancel": None,
               "pc_type": "lu",
               "pc_factor_mat_solver_type": "mumps",
               "mat_mumps_icntl_24": 1,
               "mat_mumps_icntl_13": 1
               }

        gmg  = {
               "mat_type": "matfree",
               "ksp_type": "gmres",
               "ksp_monitor_true_residual": None,
               "ksp_converged_reason": None,
               "ksp_rtol": 1.0e-8,
               "ksp_atol": 0.0,
               "pc_type": "mg",
               "mg_levels_ksp_type": "chebyshev",
               "mg_levels_ksp_max_it": 3,
               "mg_levels_ksp_convergence_test": "skip",
               "mg_levels_pc_type": "python",
               "mg_levels_pc_python_type": "firedrake.AssembledPC",
               "mg_levels_assembled_mat_type": "baij",
               "mg_levels_assembled_pc_type": "sor",
               "mg_coarse_ksp_type": "preonly",
               "mg_coarse_pc_type": "python",
               "mg_coarse_pc_python_type": "firedrake.AssembledPC",
               "mg_coarse_assembled_mat_type": "aij",
               "mg_coarse_assembled_ksp_type": "preonly",
               "mg_coarse_assembled_pc_type": "lu",
               "mg_coarse_assembled_pc_factor_mat_solver_type": "mumps",
               }

        if nref == 0:
            choice = lu
        else:
            choice = gmg

        common.update(choice)
        return common

    def refine(self, mesh, gamma):
        spacing = self.L/self.n
        invsqgm = 1/sqrt(gamma)

        if invsqgm < spacing:
            self.n *= 2
            return True
        else:
            return False

    def number_solutions(self, params):
        eps = params
        if eps < 0.03:
            return 1
        if eps < 0.07:
            return 3
        if eps < 0.12:
            return 5
        if eps < 0.18:
            return 7
        if eps < 0.20:
            return 9
        return float("inf")

    def maximum_refinements(self, params):
        return float("inf")

    def update_gamma(self, u, gamma, infeasibility, k, params):
        next_gamma = MYVIProblem.update_gamma(self, u, gamma, infeasibility, k, params)
        c = 2 # set larger for more conservative update
        return (c*gamma + next_gamma) / (c+1) # more conservative update

    def save_solution(self, filename, number, u, gamma, params):
        if gamma < 999999: return

        mesh = u.function_space().mesh()
        (lb, ub) = map(float, self.bounds(u.function_space().mesh(), params))

        lbcoords = [(x, 0) for x in self.biactivity]
        ubcoords = [(x, self.h) for x in self.biactivity]

        tol = 1.0e-4
        lbvals = [x[1] for x in u.at(lbcoords, tolerance=tol)]
        ubvals = [x[1] for x in u.at(ubcoords, tolerance=tol)]

        lbplus = +float(gamma)*numpy.maximum([lb - x for x in lbvals], 0)
        ubplus = -float(gamma)*numpy.maximum([x - ub for x in ubvals], 0)

        lbchar = numpy.zeros_like(lbplus); lbchar[lbplus > 0] = +1
        ubchar = numpy.zeros_like(ubplus); ubchar[ubplus < 0] = -1

        if mesh.mpi_comm().rank == 0:
            plt.figure(figsize=(20, 20), dpi=200)
            plt.plot(self.biactivity, lbplus, label="Multiplier for the lower bound")
            plt.plot(self.biactivity, lbchar, label="Characteristic for the lower bound")
            plt.plot(self.biactivity, ubplus, label="Multiplier for the upper bound")
            plt.plot(self.biactivity, ubchar, label="Characteristic for the upper bound")
            plt.grid()
            plt.legend(loc="best")
            plt.xlabel(r"$x$")
            plt.ylabel(r"multiplier")
            plt.savefig(filename + "multipliers-%d.png" % number)
            print("Wrote ", filename + "multipliers-%d.png" % number)
            plt.clf()
            plt.close()

if __name__ == "__main__":

    from numpy import arange
    eps_end = 0.15
    params = list(arange(0.0, eps_end, 0.001)) + [eps_end]
    problem = Beam2DProblem()
    solutions = None
    gamma_end = 100
    gamma_start = 10

    (lbc, ubc, leftwall) = problem.visualise_bounds()
    File("output/bounds/lowerbound.pvd").write(lbc)
    File("output/bounds/upperbound.pvd").write(ubc)
    File("output/bounds/leftwall.pvd").write(leftwall)

    for eps in params:
        info_red("Considering eps = %s" % eps)
        if eps == eps_end:
            gamma_end = 1e6
        solutions = solvevi(problem, eps, initial_guesses=solutions, gamma_start=gamma_start, gamma_end=gamma_end)
        gamma_start = gamma_end
        pvd = File("output/solutions-eps-%s/solutions.pvd" % eps)
        for soln in solutions:
            problem.save_pvd(pvd, soln)
