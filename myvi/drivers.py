# -*- coding: utf-8 -*-
from firedrake import *
from firedrake.mg.utils import get_level
from .misc import *
from .mlogging import *
from .deflation import defsolve
import os
import gc
gc.disable()

def solvevi(problem, params=None, refine=True, comm=COMM_WORLD, k_max=100, gamma_start=10, gamma_end=1e6, initial_guesses=None):

    os.makedirs("output", exist_ok=True)

    k = 1 # Step counter

    prev_gamma = Constant(0)
    gamma = Constant(gamma_start)

    if initial_guesses is None:
        base = problem.base_mesh(comm=comm)
        nref = 0 # Number of refinements
        mesh = base
        ele = problem.finite_element(mesh.ufl_cell())
        V = FunctionSpace(mesh, ele)
        guesses = problem.initial_guesses(V, params)
    else:
        V = initial_guesses[0].function_space()
        ele = V.ufl_element()
        mesh = V.mesh()
        nref = get_level(mesh)[1] or 0
        guesses = [guess.copy(deepcopy=True) for guess in initial_guesses]


    feasible_solutions = []

    deflation = problem.deflation_operator(params)
    (lb, ub)  = problem.bounds(mesh, params)

    max_refine = problem.maximum_refinements(params)
    max_solutions = problem.number_solutions(params)

    # Main loop
    while True:
        info_blue("Considering gamma = %s" % float(gamma))
        deflation.deflate([])

        # Refinement loop
        if refine:
            while problem.refine(mesh, float(gamma)):
                if nref + 1 > max_refine:
                    info_red("Want to refine, but limited by maximum refinements")
                    refine = False
                    break

                nref += 1
                # Need to make a new base mesh to make a new hierarchy;
                # otherwise firedrake dies
                base = problem.base_mesh(comm=comm)
                mh = MeshHierarchy(base, nref)
                mesh = mh[-1]
                prev = mh[-2]
                V_next = FunctionSpace(mesh, ele)
                V_prev = FunctionSpace(prev, ele)
                problem.build_prolongation(ele, params)
                info_blue("Refining to level %d, degrees of freedom: %d" % (nref, V_next.dim()))

                new_guesses = []
                for guess in guesses + feasible_solutions:
                    guess_prolong = Function(V_next)
                    guess_reinterp = Function(V_prev)
                    # Need to copy between the data on the old mesh
                    # in the old hierarchy and the old mesh in the
                    # new hierarchy: without this firedrake issues a
                    # "can't transfer between functions from different hierarchies"
                    with guess.dat.vec_ro as src, guess_reinterp.dat.vec_wo as dest:
                        src.copy(dest)

                    problem.prolong(guess_reinterp, guess_prolong, params)
                    new_guesses.append(guess_prolong)

                V = V_next
                guesses = new_guesses
                feasible_solutions = []
                (lb, ub)  = problem.bounds(mesh, params)

        filename = "output/params-%s-gamma-%s/" % (params, float(gamma))
        pvd = File("output/params-%s-gamma-%s/solutions.pvd" % (params, float(gamma)))

        u = Function(V)
        nvs = problem.solver(u, lb, ub, gamma, nref, params)

        active_guesses = [guess.copy(deepcopy=True) for guess in guesses]

        solutions = []
        gammas = []

        k += 1
        num_solution = -1

        num_ksp_its = 0
        num_snes_its = 0

        # Loop over guesses
        while len(active_guesses) > 0:
            guess = active_guesses.pop(0)
            u.assign(guess)

            (success, its, lits) = defsolve(nvs, deflation)

            gc.collect()

            while success and (its == 0):
                nvs.snes.atol *= 0.1
                info_red("Succeeded with no iterations, refining tolerances")
                (success, its, lits) = defsolve(nvs, deflation)

            if success:
                num_snes_its += its
                num_ksp_its  += lits

                num_solution += 1
                active_guesses.append(guess)

                problem.save_pvd(pvd, u)
                problem.save_solution(filename, num_solution, u, float(gamma), params)

                infeasibility = assemble(problem.penalty(u, lb, ub, gamma, params))
                info_green("Found solution %d for gamma = %s on refinement %d: infeasibility %.14e" % (num_solution, float(gamma), nref, infeasibility))

                if infeasibility == 0:
                    feasible_solutions.append(u.copy(deepcopy=True))
                else:
                    solutions.append(u.copy(deepcopy=True))

                next_gamma = problem.update_gamma(u, float(gamma), infeasibility, k, params)
                gammas.append(next_gamma)

                deflation.deflate(solutions + feasible_solutions)

                if len(feasible_solutions + solutions) >= max_solutions:
                    info_red("Not deflating as we have found the maximum number of solutions")
                    break

        # Finished loop over guesses
        info_red("For (params, gamma) = (%s, %s): %d feasible %s, %d infeasible %s" % (params, float(gamma), len(feasible_solutions), soln(feasible_solutions), len(solutions), soln(solutions)))
        if num_snes_its > 0:
            avg_its = "%.2lf" % (num_ksp_its/num_snes_its)
        else:
            avg_its = "∞"
        info_blue("For (params, gamma) = (%s, %s): %d total SSN iterations, %s avg Krylov iterations" % (params, float(gamma), num_snes_its, avg_its))

        if len(feasible_solutions + solutions) > 0:
            prev_gamma = gamma
            gammas.append(gamma_end)
            gamma = Constant(min(gammas))
            guesses = solutions
        else:
            gamma = (gamma + prev_gamma) / 2

        gc.collect()

        if k == k_max:
            info_blue("Terminating because k = k_max")
            break
        if float(prev_gamma) >= float(gamma_end) or float(gamma) == float("inf"):
            info_blue("Terminating because we have reached target gamma")
            break

    return solutions + feasible_solutions
