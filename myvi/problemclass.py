from firedrake import *
import firedrake
from .deflation import ShiftedDeflation
from functools import partial

class MYVIProblem(object):
    def finite_element(self, cell):
        raise NotImplementedError

    def base_mesh(self, comm):
        raise NotImplementedError

    def initial_guesses(self, V, params):
        raise NotImplementedError

    def energy(self, u, params):
        raise NotImplementedError

    def lagrangian(self, u, params):
        return self.energy(u, params)

    def penalty(self, u, lb, ub, gamma, params):
        raise NotImplementedError

    def residual(self, u, v, lb, ub, gamma, params):
        J = self.lagrangian(u, params) + self.penalty(u, lb, ub, gamma, params)
        F = derivative(J, u, v)
        return F

    def boundary_conditions(self, V, params):
        raise NotImplementedError

    def bounds(self, mesh, params):
        raise NotImplementedError

    def squared_norm(self, u, v, params):
        return inner(u - v, u - v)*dx

    def solver_parameters(self, gamma, nref, params):
        raise NotImplementedError

    def refine(self, mesh, gamma):
        raise NotImplementedError

    def prolong(self, u, v, params):
        firedrake.prolong(u, v)

    def restrict(self, u, v, params):
        firedrake.restrict(u, v)

    def inject(self, u, v, params):
        firedrake.inject(u, v)

    def build_prolongation(self, ele, params):
        pass

    def save_pvd(self, pvd, u):
        u.rename("Solution")
        pvd.write(u)

    def nullspace(self, V, params):
        return None

    def save_solution(self, filename, number, u, gamma, params):
        pass

    def update_gamma(self, u, gamma, infeasibility, k, params):
        if infeasibility == 0:
            next_gamma = float("inf")
            return next_gamma

        functional = assemble(self.energy(u, params))
        E_k  = gamma * infeasibility / functional
        theta_k = functional + infeasibility
        C_2k = gamma**(-1) * E_k * (E_k + gamma) * theta_k
        C_1k = C_2k / E_k
        tau_k = 1/k
        next_gamma = C_2k/(tau_k * abs(C_1k - theta_k)) - E_k
        return next_gamma

    def deflation_operator(self, params):
        return ShiftedDeflation(2, 1, self.squared_norm)

    def solver(self, u, lb, ub, gamma, nref, params):
        V = u.function_space()
        F = self.residual(u, TestFunction(V), lb, ub, gamma, params)
        bcs = self.boundary_conditions(V, params)
        sp = self.solver_parameters(gamma, nref, params)
        nsp = self.nullspace(V, params)

        nvp = NonlinearVariationalProblem(F, u, bcs=bcs)
        nvs = NonlinearVariationalSolver(nvp, solver_parameters=sp, nullspace=nsp, options_prefix="myvi")

        prolong = partial(self.prolong, params=params)
        restrict = partial(self.restrict, params=params)
        inject = partial(self.inject, params=params)

        ctx = dmhooks.transfer_operators(V, prolong=prolong, restrict=restrict, inject=inject)
        nvs.set_transfer_operators(ctx)

        return nvs

    def maximum_refinements(self, params):
        return float("inf")

    def number_solutions(self, params):
        return float("inf")
