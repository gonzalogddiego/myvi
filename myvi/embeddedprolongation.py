from firedrake import *
from firedrake.petsc import *

orig_prolong = prolong
orig_restrict = restrict
orig_inject = inject

__all__ = ["EmbeddedProlongation"]

class EmbeddedProlongation(object):
    """
    Enable prolongation, restriction and injection for
    non-Lagrange elements (Raviart-Thomas, Nedelec, Argyris,
    etc) by embedding the function into a containing DG function
    space of the right order. This is slow but should enable
    investigation of multigrid convergence until a better
    implementation is available.
    """

    def __init__(self, ele):

        cell = ele.cell()
        shape = ele.value_shape()
        if len(shape) == 0:
            eleDG = FiniteElement("DG", cell, ele.degree())
        elif len(shape) == 1:
            eleDG = VectorElement("DG", cell, ele.degree(), dim=shape[0])
        elif len(shape) == 2:
            eleDG = TensorElement("DG", cell, ele.degree(), shape=shape)

        self.eleDG = eleDG

        self.mixed_mass_matrices = {}
        self.inverse_DG_mass_matrices = {}
        self.mass_matrix_solvers = {}
        self.scratch_V = {}
        self.scratch_DG = {}
        self.scratch_2_DG = {}

    def setup(self, uc, uf):

        Vc = uc.function_space()
        meshc = Vc.mesh()
        VcDG = FunctionSpace(meshc, self.eleDG)

        Vf = uf.function_space()
        meshf = Vf.mesh()
        VfDG = FunctionSpace(meshf, self.eleDG)

        coarsekey = Vc.dim()
        if coarsekey not in self.mixed_mass_matrices:
            # We'll need to assemble a mixed mass matrix.
            M = assemble(inner(TestFunction(VcDG), TrialFunction(Vc))*dx)
            M.force_evaluation()
            self.mixed_mass_matrices[coarsekey] = M.petscmat

        if coarsekey not in self.inverse_DG_mass_matrices:
            # We'll also need the inverse DG mass matrix.
            M = assemble(Tensor(inner(TestFunction(VcDG), TrialFunction(VcDG))*dx).inv)
            M.force_evaluation()
            self.inverse_DG_mass_matrices[coarsekey] = M.petscmat

        if coarsekey not in self.scratch_DG:
            self.scratch_DG[coarsekey] = Function(VcDG)

        if coarsekey not in self.scratch_2_DG:
            self.scratch_2_DG[coarsekey] = Function(VcDG)

        finekey = Vf.dim()
        if finekey not in self.mixed_mass_matrices:
            # We'll need to assemble a mixed mass matrix.
            M = assemble(inner(TestFunction(VfDG), TrialFunction(Vf))*dx)
            M.force_evaluation()
            self.mixed_mass_matrices[finekey] = M.petscmat

        if finekey not in self.mass_matrix_solvers:
            ele = Vf.ufl_element()
            M = assemble(inner(TestFunction(Vf), TrialFunction(Vf))*dx)
            M.force_evaluation()
            kspM = PETSc.KSP().create(comm=Vc.mesh().mpi_comm())
            kspM.setOperators(M.petscmat)
            kspM.setOptionsPrefix("%s_prolongation_mass_" % ele._short_name)
            kspM.setType("preonly")
            kspM.pc.setType("cholesky")
            kspM.setFromOptions()
            kspM.setUp()
            self.mass_matrix_solvers[finekey] = kspM

        if finekey not in self.scratch_DG:
            self.scratch_DG[finekey] = Function(VfDG)

        if finekey not in self.scratch_2_DG:
            self.scratch_2_DG[finekey] = Function(VfDG)

        if finekey not in self.scratch_V:
            self.scratch_V[finekey] = Function(Vf)

    def op(self, uc, uf, op):
        # u in V/coarse -> u in DG/coarse -> u in DG/fine -> u in V/fine
        self.setup(uc, uf)

        Vc = uc.function_space()
        Vf = uf.function_space()

        coarsekey = Vc.dim()
        mixedc = self.mixed_mass_matrices[coarsekey]
        DGcmassinv = self.inverse_DG_mass_matrices[coarsekey]

        finekey = Vf.dim()
        mixedf = self.mixed_mass_matrices[finekey]
        Vfmassksp = self.mass_matrix_solvers[finekey]

        DGcscratch = self.scratch_DG[coarsekey]
        DGcscratch2 = self.scratch_2_DG[coarsekey]
        DGfscratch = self.scratch_DG[finekey]
        Vfscratch = self.scratch_V[finekey]

        # u in V/coarse -> u in DG/coarse
        with uc.dat.vec_ro as ucv, DGcscratch2.dat.vec_wo as DGcscratch2v:
            mixedc.mult(ucv, DGcscratch2v)

        with DGcscratch.dat.vec_wo as DGcscratchv, DGcscratch2.dat.vec_ro as DGcscratch2v:
            DGcmassinv.mult(DGcscratch2v, DGcscratchv)

        # u in DG/coarse -> u in DG/fine
        op(DGcscratch, DGfscratch)

        # u in DG/fine -> u in V/fine
        with DGfscratch.dat.vec_ro as DGfscratchv, Vfscratch.dat.vec as Vfscratchv, uf.dat.vec_wo as ufv:
            mixedf.multTranspose(DGfscratchv, Vfscratchv)
            Vfmassksp.solve(Vfscratchv, ufv)

    def prolong(self, uc, uf):
        self.op(uc, uf, op=orig_prolong)

    def inject(self, uf, uc):
        self.op(uf, uc, op=orig_inject)

    def restrict(self, gf, gc):
        self.setup(gc, gf)

        Vc = gc.function_space()
        Vf = gf.function_space()

        coarsekey = Vc.dim()
        mixedc = self.mixed_mass_matrices[coarsekey]
        DGcmassinv = self.inverse_DG_mass_matrices[coarsekey]

        finekey = Vf.dim()
        mixedf = self.mixed_mass_matrices[finekey]
        Vfmassksp = self.mass_matrix_solvers[finekey]

        DGcscratch = self.scratch_DG[coarsekey]
        DGcscratch2 = self.scratch_2_DG[coarsekey]
        DGfscratch = self.scratch_DG[finekey]
        Vfscratch = self.scratch_V[finekey]

        with gf.dat.vec_ro as gfv, Vfscratch.dat.vec_wo as ufv:
            Vfmassksp.solve(gfv, ufv)

        with Vfscratch.dat.vec_ro as ufv, DGfscratch.dat.vec_wo as DGfscratchv:
            mixedf.mult(ufv, DGfscratchv)

        orig_restrict(DGfscratch, DGcscratch)

        with DGcscratch.dat.vec_ro as DGcscratchv, DGcscratch2.dat.vec_wo as DGcscratch2v:
            DGcmassinv.mult(DGcscratchv, DGcscratch2v)

        with DGcscratch2.dat.vec_ro as DGcscratch2v, gc.dat.vec_wo as gcv:
            mixedc.multTranspose(DGcscratch2v, gcv)
